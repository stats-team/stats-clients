<?php

namespace StatsClient;

class Api()
{
	const API_URL = 'http://api.statslocal.com';

	protected static function makeRequest($url, $params)
	{
		foreach ($params as $key => &$val) {
			if (is_array($val)) $val = implode(',', $val);
			$post_params[] = $key.'='.urlencode($val);
		}

		$post_string = implode('&', $post_params);
		$parts=parse_url($url);
		$port = isset($parts['port']) ? $parts['port'] : 80;

		$fp = fsockopen($parts['host'], $port, $errno, $errstr, 30);

		$out = "POST ".$parts['path']." HTTP/1.1\r\n";
		$out.= "Host: ".$parts['host']."\r\n";
		$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
		$out.= "Content-Length: ".strlen($post_string)."\r\n";
		$out.= "Connection: Close\r\n\r\n";

		if (isset($post_string)) $out.= $post_string;

		fwrite($fp, $out);
		fclose($fp);
	}

	public static function sendStatValue($userkey, $key, $value)
	{
		self::makeRequest(self::API_URL."/stats", ['apikey' => $userkey, 'key' => $key, 'value' => $value, 'type' => 'value']);
	}

	public static function sendStatCounter($userkey, $key, $counter)
	{
		self::makeRequest(self::API_URL."/stats", ['apikey' => $userkey, 'key' => $key, 'value' => $counter, 'type' => 'counter']);
	}
}